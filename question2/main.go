package main

import (
	"fmt"
)

func main() {
	main := []int{20, 7, 8, 10, 2, 5, 6, 1, 2, 3}

	fmt.Println(sequenceExists(main, []int{7, 8}))
	fmt.Println(sequenceExists(main, []int{8, 7}))
	fmt.Println(sequenceExists(main, []int{7, 10}))
	fmt.Println(sequenceExists(main, []int{1, 2, 3}))
}

//sequenceExists find a sequence number exist
func sequenceExists(main []int, data []int) bool {
	lengthOfData := len(data)
	lengthOfMain := len(main)

	for i := 0; i <= lengthOfMain-lengthOfData; i++ {
		sequenceFound := 0
		for j := 0; j < lengthOfData; j++ {
			if data[j] == main[i+j] {
				sequenceFound++
				if sequenceFound == lengthOfData {
					return true
				}
			}
		}
	}

	return false
}
