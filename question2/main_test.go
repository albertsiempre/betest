package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSequenceExists(t *testing.T) {
	data := []int{20, 7, 8, 10, 2, 5, 6, 1, 2, 3}

	tests := []struct {
		name     string
		request  []int
		expected bool
	}{
		{
			name:     "Scenario 1",
			request:  []int{7, 8},
			expected: true,
		},
		{
			name:     "Scenario 2",
			request:  []int{8, 7},
			expected: false,
		},
		{
			name:     "Scenario 3",
			request:  []int{7, 10},
			expected: false,
		},
		{
			name:     "Scenario 4",
			request:  []int{1, 2, 3},
			expected: true,
		},
	}

	for _, test := range tests {
		res := sequenceExists(data, test.request)
		assert.Equal(t, test.expected, res, test.name)
	}
}
