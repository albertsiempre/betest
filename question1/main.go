package main

import "fmt"

type Node struct {
	value int
	left  *Node
	right *Node
}

func main() {
	tree := &Node{value: 6}

	tree.left = &Node{value: 7}
	tree.left.left = &Node{value: 10}
	tree.left.right = &Node{value: 8}
	tree.right = &Node{value: 100}
	tree.right.right = &Node{value: 14}

	fmt.Println(tree.find(6))
	fmt.Println(tree.find(7))
	fmt.Println(tree.find(8))
	fmt.Println(tree.find(9))
	fmt.Println(tree.find(10))
	fmt.Println(tree.find(14))
}

//find find the number in tree
func (node *Node) find(val int) bool {
	if node == nil {
		return false
	}

	if node.value == val {
		return true
	}

	resultLeft := node.left.find(val)
	if resultLeft {
		return true
	}

	return node.right.find(val)
}
