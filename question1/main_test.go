package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFind(t *testing.T) {
	tree := &Node{value: 6}

	tree.left = &Node{value: 7}
	tree.left.left = &Node{value: 10}
	tree.left.right = &Node{value: 8}
	tree.right = &Node{value: 100}
	tree.right.right = &Node{value: 14}

	tests := []struct {
		name     string
		request  int
		expected bool
	}{
		{
			name:     "Scenario 1",
			request:  6,
			expected: true,
		},
		{
			name:     "Scenario 2",
			request:  7,
			expected: true,
		},
		{
			name:     "Scenario 3",
			request:  8,
			expected: true,
		},
		{
			name:     "Scenario 4",
			request:  9,
			expected: false,
		},
		{
			name:     "Scenario 5",
			request:  10,
			expected: true,
		},
		{
			name:     "Scenario 6",
			request:  14,
			expected: true,
		},
	}

	for _, test := range tests {
		res := tree.find(test.request)
		assert.Equal(t, test.expected, res, test.name)
	}
}
