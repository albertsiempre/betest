## Getting Started

### Please make sure **Go** installed on your machine or you can run on play.golang.org
1. Answer for question #1, run:  `go run question1/main.go`
2. Answer for question #2, run:  `go run question2/main.go`

### Unit Test
1. Run:  `go test -v ./question1 ./question2`

### A little explanation about the answer to question No.3 with query
1. Select data user wallets >
`SELECT id, name FROM mydb.users a
INNER JOIN (
	SELECT id, reference_id, name, identifier FROM wallets where identifier = 'user'
) b ON a.id = b.reference_id`

2. Select data team wallets >
`SELECT id, name FROM mydb.teams a
INNER JOIN (
	SELECT id, reference_id, name, identifier FROM wallets where identifier = 'teams'
) b ON a.id = b.reference_id`

3. Select data card limit (daily) > `SELECT id, card_id, amount FROM card_limits where identifier = 'daily'`

4. Select data card limit (monthly) > `SELECT id, card_id, amount FROM card_limits where identifier = 'monthly'`
